import React , {useState} from 'react';
import "./main.css";

function Main(props) {
    React.useEffect(()=>{
      window.scrollTo(0,0);
    }, []);
    const [percent, setPercent]=useState("");
    window.addEventListener('scroll', ()=>{
        let progress=document.querySelector('.progressbar');
        let height=document.documentElement.scrollHeight-document.documentElement.clientHeight;
        let scrollTop=document.documentElement.scrollTop;
        setPercent(prev=>Math.floor(scrollTop/height*100));
        progress.style.width=`${scrollTop/height*100}%`;
    });
    return (
        <div className="main">
            <div className="progressbar">
                <div className="progress-hover">
                    {percent>0? percent+"%":""}
                </div>
                <div className="animation-infinite"></div>
            </div>
            <div className="title">
                Scroll Progressbar Animation
            </div>
            <div className="text-content">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad at, consectetur cumque, cupiditate earum
                eius ex facere maxime nam nemo nostrum quam quos sapiente tempore voluptates. A amet at blanditiis
                deleniti ducimus eius impedit modi nisi placeat, quae sit tempore unde ut voluptas, voluptates?
                Doloremque doloribus eius eligendi, esse et fugit impedit itaque mollitia nemo nulla omnis reiciendis
                reprehenderit soluta sunt suscipit voluptatem voluptatum? Debitis ipsum nihil officia quos temporibus.
                <br/>
                Ab accusantium ad aliquid amet aperiam, asperiores at aut blanditiis commodi consequuntur corporis culpa
                deleniti deserunt dolor dolore eaque, enim est facere fuga fugit id illum iusto laboriosam laborum
                laudantium libero minima nostrum pariatur perspiciatis possimus quia quibusdam quisquam repellat sint ut
                voluptas voluptatibus. Aliquam consequuntur culpa ea inventore ipsam nemo perferendis quia quos ratione,
                similique sunt, tempora, tempore? Dolorum eaque, enim? Amet aperiam at facilis molestiae provident! Cum
                dolor harum hic id impedit laborum laudantium natus, nostrum officiis provident quae quia quod ratione
                rerum, voluptates! Aliquid aspernatur cum debitis distinctio iusto modi molestias neque, repudiandae
                <br/>
                sequi voluptatem. Accusamus alias amet aspernatur assumenda atque beatae consectetur dicta doloremque
                doloribus esse eum harum in, ipsa laboriosam magni minus nemo neque nisi non odio omnis placeat porro
                praesentium quae quaerat quam quidem ratione rerum sapiente sunt velit veniam voluptas voluptate? A
                adipisci aliquam amet animi aut autem delectus deleniti doloremque dolores dolorum eius eligendi est
                explicabo fuga fugiat fugit harum illo illum ipsam molestiae necessitatibus numquam omnis quas quasi,
                qui ratione repellat repellendus rerum sint totam velit voluptas voluptatibus voluptatum. Aliquid
                architecto obcaecati ullam! A adipisci aperiam eligendi enim eveniet, explicabo fuga ipsam
                <br/>
                necessitatibus nobis nulla quae quam quasi, qui sit voluptatibus. Accusantium ad aut debitis deleniti
                dicta dolorem dolores dolorum ducimus est ex fugit harum illo, inventore iste iure iusto laboriosam
                libero maxime minima nemo neque non reiciendis sint sunt unde veniam vero. Ad assumenda autem cupiditate
                delectus id officia vitae voluptatibus! Animi aperiam architecto aspernatur at debitis doloribus eaque
                earum enim eos id, illum impedit ipsa laborum nostrum odio omnis possimus quas, quia quo quos sed
                temporibus veniam voluptas voluptates voluptatum? Blanditiis deserunt doloremque harum nobis
                reprehenderit, totam vero vitae voluptatum? Aut consequuntur dolor earum, magnam quas recusandae rerum
                sapiente soluta tenetur. Ex inventore modi pariatur quas sapiente temporibus! Ad blanditiis dolor ex
                fuga laudantium placeat quas quo sit unde voluptas! Commodi deleniti doloremque dolorum impedit ipsam
                magni molestiae nam nihil placeat, praesentium sint, ut voluptatibus? Accusantium architecto dolores
                eaque molestiae omnis repellendus! Consequuntur delectus, hic impedit libero nemo numquam perspiciatis
                quisquam quod repellat totam. Accusamus ad aliquam, atque corporis eius fuga ipsam iste necessitatibus
                nesciunt non optio quae quasi rem reprehenderit repudiandae sit tempora tempore veniam vitae,
                voluptatum. Consectetur consequatur consequuntur corporis, cumque, deleniti dicta doloribus earum eius
                enim, esse excepturi fugiat ipsa molestias nostrum odio pariatur provident quam quasi quidem quisquam
                rerum saepe sequi similique tempore ut veritatis voluptas voluptatem. Amet consequatur explicabo id
                <br/>
                incidunt magni. Deserunt ipsa, optio quae quaerat sed sit ullam voluptates. Facere inventore itaque
                nobis nulla obcaecati provident quia quibusdam repellat soluta? Doloribus libero molestias, quam sed
                veritatis voluptas. Ad animi commodi corporis dolore fuga fugit hic ipsum natus, neque nihil porro
                repellendus rerum veniam! Cumque dicta doloremque eius nam odit suscipit unde voluptates! Ab, adipisci
                architecto assumenda aut autem corporis distinctio eaque enim et eum excepturi id ipsum iste magni nam
                nostrum obcaecati perferendis perspiciatis porro quidem quis repellendus similique sint tempora
                temporibus veritatis voluptate. Aliquam at est impedit sed veniam! Consequuntur, repellendus,
                voluptatum. A aperiam aspernatur consequuntur doloremque eveniet illo ipsa magnam molestias numquam
                quasi, quis repudiandae tempora veritatis? Amet animi aspernatur assumenda commodi corporis cum cumque
                cupiditate debitis distinctio doloribus ducimus eius enim eveniet ex fugiat illo inventore iste libero
                minus neque nisi odit porro praesentium quibusdam quod, quos reiciendis rerum saepe tempore totam ullam
                velit veritatis voluptatem? Ad consequuntur delectus ea maxime modi placeat, voluptatum. Aliquam
                architecto et facere impedit laboriosam nam sit veniam. Accusantium ad at aut debitis dolore dolores
                ducimus expedita harum id illum libero maiores, nihil odio odit perspiciatis provident quisquam quo
                recusandae reprehenderit sint sunt suscipit tempore tenetur veritatis voluptas. Debitis eum ex hic iure
                maiores nihil numquam, omnis praesentium quas saepe suscipit ut. At cupiditate dicta fugit incidunt
                laborum pariatur possimus provident quae quos vel.
                <br/>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, aliquam animi eaque enim, eos harum itaque
                labore laudantium, maxime nulla quas quos sint suscipit tempora unde? Accusamus aliquid architecto ipsa
                nobis pariatur rem repudiandae similique! Aut, blanditiis culpa, cumque delectus eligendi expedita
                facere facilis illo iste repudiandae sit sunt tenetur ut veritatis voluptas? Alias delectus, explicabo
                facilis fugiat impedit iusto labore nisi, officia officiis perferendis perspiciatis suscipit temporibus
                vero. Accusantium, alias aperiam at autem cumque delectus doloremque earum enim error fugit illum
                impedit incidunt iste iusto maiores maxime minima minus neque pariatur placeat porro provident quam
                quasi quia quidem rerum sequi sit tempore voluptas voluptatibus! Blanditiis dolor eos esse fugit in iure
                necessitatibus officiis quas saepe? Architecto aspernatur aut corporis debitis distinctio doloremque
                ducimus eius eligendi ex, fuga magni necessitatibus odio officiis pariatur quos rem voluptate! Aut
                inventore maiores ut? Ea, pariatur, repudiandae? Aliquid cupiditate doloremque eius facere itaque
                laudantium magnam voluptatem. Consequuntur fugit quidem quos rerum sit ut. Ab accusamus architecto aut
                autem commodi cupiditate delectus deleniti dolor dolores enim error et expedita hic illo illum
                <br/>
                laudantium molestias obcaecati optio perspiciatis possimus quam, quidem quod repudiandae suscipit,
                vitae! Ad enim eum fugiat molestiae perspiciatis ratione, sint sit suscipit totam unde! Alias aut
                distinctio dolorem dolores eius esse est ex explicabo harum, ipsa ipsam iusto necessitatibus neque
                nostrum possimus quae tenetur! Accusamus enim rerum saepe sit veniam. Consequuntur cupiditate dolorem
                doloremque modi necessitatibus nostrum possimus sit! Aut commodi consequatur deleniti dolore ea earum id
                iste molestias mollitia, neque obcaecati odio, saepe sed suscipit, unde! Aliquam assumenda culpa,
                doloribus ex in nobis perspiciatis placeat repellat tempore velit. Accusantium at eaque eveniet in optio
                praesentium sed tenetur? Accusantium assumenda consequuntur molestias voluptatibus? Aliquid beatae
                blanditiis dignissimos distinctio eligendi ex fugiat illo mollitia perferendis ratione, repudiandae
                rerum ullam, vero? A accusamus amet animi autem, beatae consectetur consequuntur deleniti distinctio
                <br/>
                dolor doloremque doloribus eaque eius eligendi excepturi exercitationem explicabo fugiat illo, in libero
                magni minus molestiae molestias, neque nesciunt perferendis quam quas quasi qui quibusdam rem
                repudiandae sint sunt velit veritatis vitae voluptatem voluptates? Aut, corporis, debitis dolor esse eum
                iusto labore nam nemo numquam, odio omnis quae quasi reiciendis sapiente voluptate. Cumque distinctio,
                dolorum eius explicabo repudiandae vel voluptatibus. A assumenda commodi dignissimos ducimus error
                expedita facere facilis in ipsum nulla odio odit praesentium quisquam reiciendis rem reprehenderit saepe
                sint ullam vitae, voluptate? Aliquam animi aut autem cum, dignissimos dolore earum facere ipsam, laborum
                laudantium minima neque nobis perspiciatis recusandae sint vel velit. A dolor dolore error
                <br/>
                exercitationem laudantium nesciunt omnis perspiciatis possimus qui quis rerum velit voluptas,
                voluptatibus. A ab consectetur, consequatur cumque debitis deserunt ducimus earum enim eos est
                exercitationem facere facilis fuga hic id impedit incidunt ipsa iure laboriosam molestias nam neque
                obcaecati odio officiis perferendis perspiciatis, quam quia quidem rem saepe sequi sit suscipit tempora
                unde vel velit voluptas. Architecto cum dignissimos doloremque ducimus, eius enim expedita illum minus
                nam nisi omnis recusandae unde. Accusamus atque dolor dolorem ea, eaque eum facilis hic natus neque odit
                porro quae quaerat quas qui soluta. Aperiam aut dolorem doloribus ea eaque eos excepturi exercitationem
                fugit illo libero maiores maxime molestiae nam nemo nobis nostrum numquam odio officia provident, quae
                <br/>
                quam quasi ratione rem soluta tempora tenetur vitae! Accusamus alias consequatur cupiditate ducimus hic
                minima modi nam nisi non, porro quae, quaerat qui quia quibusdam sapiente velit voluptatem. Debitis
                dolorum, ea eum eveniet obcaecati praesentium quam sed. Amet assumenda cum dolore eaque eligendi nihil
                quas vitae? Asperiores consequatur debitis dolor doloremque ea est et facere illum, in inventore ipsam
                minima nobis non odit, officiis optio pariatur, quibusdam reprehenderit saepe tenetur. Atque illum minus
                quae! Assumenda autem consectetur consequatur cumque delectus facere fuga harum illo iure iusto nemo
                omnis placeat porro quaerat quis, reprehenderit repudiandae sint suscipit? Aspernatur aut beatae
                consequatur dolor impedit libero nobis porro. Asperiores at, blanditiis consequuntur deserunt distinctio
                dolores esse est impedit, maiores natus obcaecati odio possimus quasi quisquam quos rem rerum saepe
                <br/>
                tempora, voluptas voluptatibus. Ad beatae distinctio dolor eaque est eveniet facilis harum id in iusto
                nobis qui quibusdam ratione rem, saepe suscipit tempora temporibus? Accusantium architecto aspernatur at
                cumque cupiditate distinctio doloremque dolores eaque enim error eum eveniet excepturi, exercitationem
                fuga, impedit in incidunt iste iusto laboriosam mollitia necessitatibus nesciunt odit optio perspiciatis
                porro provident quas recusandae reprehenderit sit tempora tempore totam ullam veritatis vitae voluptas
                voluptatem voluptates! Amet consequuntur culpa distinctio earum fugit illo impedit laboriosam magni
                maiores nostrum, nulla perferendis quasi rem repellat, saepe vero voluptates. Enim nam officiis tenetur?
                Ab accusamus accusantium amet asperiores autem culpa cupiditate dolores doloribus, earum error esse
                facilis fugiat labore maiores non pariatur porro quod ratione reiciendis repellat sapiente temporibus
                vero vitae. Laborum libero magnam magni quasi quidem quod quos suscipit voluptatem! Aperiam autem,
                consectetur corporis deleniti dolore, enim excepturi ipsam iste libero, officiis qui rerum suscipit
                voluptas voluptatibus.
            </div>
        </div>
    );
}

export default Main;